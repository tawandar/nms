/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nust.systems.workingcrud.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import nust.systems.workingcrud.config.PersistanceUnit;
import nust.systems.workingcrud.dao.exceptions.NonexistentEntityException;
import nust.systems.workingcrud.entities.NUSTUSDApplicantChoice;
import nust.systems.workingcrud.entities.NUSTUSDApplicantChoicePK;

/**
 *
 * @author Rodney
 */
public class NUSTUSDApplicantChoiceDao {
    PersistanceUnit pu = PersistanceUnit.getInstance();

    public NUSTUSDApplicantChoiceDao(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public NUSTUSDApplicantChoiceDao() {
    }

    private EntityManagerFactory emf = pu.enFactory;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NUSTUSDApplicantChoice nustusdapplicantchoice) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(nustusdapplicantchoice);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
      public void edit(NUSTUSDApplicantChoice nustusdapplicantchoice) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            nustusdapplicantchoice = em.merge(nustusdapplicantchoice);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                NUSTUSDApplicantChoicePK nustusdapplicantchoicepk ;
                nustusdapplicantchoicepk = nustusdapplicantchoice.getNUSTUSDApplicantChoicePK();
                if (findApplicant(nustusdapplicantchoicepk) == null) {
                    throw new NonexistentEntityException("The student with id " + nustusdapplicantchoicepk + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NUSTUSDApplicantChoice> findChoiceEntities() {
        return findChoiceEntities(true, -1, -1);
    }

    public List<NUSTUSDApplicantChoice> findChoiceEntities(int maxResults, int firstResult) {
        return findChoiceEntities(false, maxResults, firstResult);
    }

    private List<NUSTUSDApplicantChoice> findChoiceEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(NUSTUSDApplicantChoice.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NUSTUSDApplicantChoice findApplicant(NUSTUSDApplicantChoicePK nustusdapplicantchoicepk) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NUSTUSDApplicantChoice.class, nustusdapplicantchoicepk);
        } finally {
            em.close();
        }
    }
    
    public void destroy(NUSTUSDApplicantChoicePK nustusdapplicantchoicepk) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NUSTUSDApplicantChoice nustusdapplicantchoice;
            try {
                nustusdapplicantchoice = em.getReference(NUSTUSDApplicantChoice.class, nustusdapplicantchoicepk);
                nustusdapplicantchoice.getNUSTUSDApplicantChoicePK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The student with id " + nustusdapplicantchoicepk + " no longer exists.", enfe);
            }
            em.remove(nustusdapplicantchoice);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    } 
}
