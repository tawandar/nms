/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nust.systems.workingcrud.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import nust.systems.workingcrud.config.PersistanceUnit;
import nust.systems.workingcrud.entities.NUSTUSDApplicant;

/**
 *
 * @author Rodney
 */
public class NUSTUSDApplicantDao {

    PersistanceUnit pu = PersistanceUnit.getInstance();

    public NUSTUSDApplicantDao(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public NUSTUSDApplicantDao() {
    }

    private EntityManagerFactory emf = pu.enFactory;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NUSTUSDApplicant nustusdapplicant) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(nustusdapplicant);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NUSTUSDApplicant> findStudentEntities() {
        return findStudentEntities(true, -1, -1);
    }

    public List<NUSTUSDApplicant> findStudentEntities(int maxResults, int firstResult) {
        return findStudentEntities(false, maxResults, firstResult);
    }

    private List<NUSTUSDApplicant> findStudentEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(NUSTUSDApplicant.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NUSTUSDApplicant findApplicant(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NUSTUSDApplicant.class, id);
        } finally {
            em.close();
        }
    }
}
