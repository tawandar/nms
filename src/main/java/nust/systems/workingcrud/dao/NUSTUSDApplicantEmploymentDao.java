/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nust.systems.workingcrud.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
//import javax.persistence.PersistenceUnit;
import nust.systems.workingcrud.config.PersistanceUnit;
import nust.systems.workingcrud.entities.NUSTUSDApplicantEmployment;

/**
 *
 * @author systems1
 */
public class NUSTUSDApplicantEmploymentDao implements Serializable {
    
    PersistanceUnit pu = PersistanceUnit.getInstance();
    
    public NUSTUSDApplicantEmploymentDao(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public NUSTUSDApplicantEmploymentDao() {
    }
    
    private EntityManagerFactory emf = pu.enFactory;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public void create(NUSTUSDApplicantEmployment nustusdapplicantemployment) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(nustusdapplicantemployment);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
