/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nust.systems.workingcrud.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import nust.systems.workingcrud.config.PersistanceUnit;
import nust.systems.workingcrud.dao.NUSTUSDApplicantDao;
import nust.systems.workingcrud.entities.NUSTUSDApplicant;
import nust.systems.workingcrud.entities.NUSTUSDApplicantChoice;
import nust.systems.workingcrud.entities.NUSTUSDApplicantQualification;
import org.springframework.stereotype.Controller;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Rodney
 */

@Controller
public class NUSTApplicantView {
    
    NUSTUSDApplicantDao app = new NUSTUSDApplicantDao();
    
    @RequestMapping(value = "/")
     public ModelAndView viewApplicant(ModelAndView model) throws IOException {
        
     System.out.println("Student name: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" );
     NUSTUSDApplicant ab= new NUSTUSDApplicant();;
     ab.getCapturedBy()
     String search="13592";
                 if(findNo(search)!=null){
        ab=findNo(search);
       List<NUSTUSDApplicantQualification> qqq= findQualification(ab.getNUSTUSDApplicantPK().getNo());
       List<NUSTUSDApplicantChoice> choices= findChoices(ab.getNUSTUSDApplicantPK().getNo());
       int points=getAlevelpoints(ab.getNUSTUSDApplicantPK().getNo());
       int noQualifications=qqq.size();
       int noChoices=choices.size();
       model.addObject("pointsout", points);
       model.addObject("count", noQualifications);
       model.addObject("choicecount", noChoices);
       model.addObject("applicant", ab);
       model.setViewName("view_applicant");
       return model;
}
        
        else if(findCustNo(search)!=null){
            ab=findCustNo(search);
                 model.addObject("applicant", ab);
     model.setViewName("view_applicant");
     return model;
}
        
        else if(findSurname(search)!=null){
        ab=findSurname(search);
             model.addObject("applicant", ab);
     model.setViewName("view_applicant");
     return model;
}
        
        else if(findName(search)!=null){
        ab=findName(search);
             model.addObject("applicant", ab);
     model.setViewName("view_applicant");
     return model;
}
        
        else{
        System.out.println("Search not found");}
    
     System.out.println("Student name SSSSSSSSSSSSSSSSSSS"+ab.getForenames());
     System.out.println("Student number  SSSSSSSSSSSSSSSSSSS"+ab.getNUSTUSDApplicantPK().getCustomerNo() );
     System.out.println("Student applicant  SSSSSSSSSSSSSSSSSSS"+ab.getNUSTUSDApplicantPK().getNo() );
return model;
    }
     
     /* View Qualifications */
         @RequestMapping(value = "/viewQualifications", method=RequestMethod.GET)
     public ModelAndView viewQualifications(@RequestParam(value="id", required=true) String id) throws IOException {
    System.out.println("Now printing Qualifications");
    ModelAndView model=new ModelAndView();
    System.out.println("gfhjjjjjjjjjjjj"+id);
  //  List<NUSTUSDApplicantQualification> qltns=findQualification
     model.setViewName("index");
     return model;
     }
     
     /* View Qualifications */
         @RequestMapping(value = "/viewChoices", method=RequestMethod.GET)
     public ModelAndView viewChoices(@RequestParam(value="id", required=true) String id) throws IOException {
    System.out.println("Now printing Qualifications");
    ModelAndView model=new ModelAndView();
    System.out.println("gfhjjjjjjjjjjjj"+id);
  //  List<NUSTUSDApplicantQualification> qltns=findQualification
     model.setViewName("index");
     return model;
     }
     
     
     /* Get A-level point */
     public int getAlevelpoints(String search){
        System.out.println("Now in Points Calculation ");
        int points=0;
        int level=2;
        PersistanceUnit pu = PersistanceUnit.getInstance();
        EntityManagerFactory emf = pu.enFactory;        
        EntityManager em = emf.createEntityManager();
        NUSTUSDApplicantQualification ab= new NUSTUSDApplicantQualification();
        Query query5 = em.createQuery("SELECT n FROM NUSTUSDApplicantQualification n WHERE n.nUSTUSDApplicantQualificationPK.applicantNo = :applicantNo");
        query5.setParameter("applicantNo", search);
        List<NUSTUSDApplicantQualification> resultList5 = query5.getResultList();
        List<NUSTUSDApplicantQualification> qout=new ArrayList<NUSTUSDApplicantQualification>();
        em.close();
        for(NUSTUSDApplicantQualification qq: resultList5){
        if(!qq.getProgrammeDescription().isEmpty()){
        qout.add(qq);
        }
        }
        for(NUSTUSDApplicantQualification qs: qout){
        if(qs.getLevel()==level){
        points=points+qs.getSubjectPoints();
        }
        
        }
        
        System.out.println("Points are"+points);
     return points;
     }
     
     
      /* Find Applicant by Applicant Number */
    public NUSTUSDApplicant findNo(String search){
        PersistanceUnit pu = PersistanceUnit.getInstance();
        EntityManagerFactory emf = pu.enFactory;        
        EntityManager em = emf.createEntityManager();
        NUSTUSDApplicant ab= new NUSTUSDApplicant();
        Query query1 = em.createQuery("SELECT n FROM NUSTUSDApplicant n WHERE n.nUSTUSDApplicantPK.no = :no");
        query1.setParameter("no", search);
        List<NUSTUSDApplicant> resultList1 = query1.getResultList();
        for(NUSTUSDApplicant aa:  resultList1){
        System.out.println("xxxxxxxxxxxxxxxxxx  "+aa.getForenames());
        ab=aa;}
        em.close();
        return ab;
    }
    
    
    
    /* Find Applicant by Customer Number */
    public NUSTUSDApplicant findCustNo(String search){
        PersistanceUnit pu = PersistanceUnit.getInstance();
        EntityManagerFactory emf = pu.enFactory;        
        EntityManager em = emf.createEntityManager();
        NUSTUSDApplicant ab= new NUSTUSDApplicant();
        Query query2 = em.createQuery("SELECT n FROM NUSTUSDApplicant n WHERE n.nUSTUSDApplicantPK.customerNo = :customerNo");
        query2.setParameter("customerNo", search);
        List<NUSTUSDApplicant> resultList2 = query2.getResultList();
        for(NUSTUSDApplicant aa:  resultList2){
        System.out.println("xxxxxxxxxxxxxxxxxx"+aa.getForenames());}
        em.close();
        return ab;
    }
    
    /* Find Applicant by Surname */    
    public NUSTUSDApplicant findSurname(String search){
        PersistanceUnit pu = PersistanceUnit.getInstance();
        EntityManagerFactory emf = pu.enFactory;        
        EntityManager em = emf.createEntityManager();
        NUSTUSDApplicant ab= new NUSTUSDApplicant();
        Query query3 = em.createQuery("SELECT n FROM NUSTUSDApplicant n WHERE n.surname = :surname");
        query3.setParameter("surname",search);
        List<NUSTUSDApplicant> resultList3 = query3.getResultList();
        for(NUSTUSDApplicant aa:  resultList3){
        System.out.println("xxxxxxxxxxxxxxxxxx"+aa.getForenames());}
        em.close();
        return ab;
    }
    
    /* Find Applicant by Name */
    public NUSTUSDApplicant findName(String search){
        PersistanceUnit pu = PersistanceUnit.getInstance();
        EntityManagerFactory emf = pu.enFactory;        
        EntityManager em = emf.createEntityManager();
        NUSTUSDApplicant ab= new NUSTUSDApplicant();
        Query query4 = em.createQuery("SELECT n FROM NUSTUSDApplicant n WHERE n.forenames = :forenames");
        query4.setParameter("forenames", search);
        List<NUSTUSDApplicant> resultList4 = query4.getResultList();
        for(NUSTUSDApplicant aa:  resultList4){
        System.out.println("xxxxxxxxxxxxxxxxxx"+aa.getForenames());}
        em.close();
        return ab;
    }
    
    
    /* Find qualifications */
        public List<NUSTUSDApplicantQualification> findQualification(String search){
        System.out.println("Now in findQualification ");
        PersistanceUnit pu = PersistanceUnit.getInstance();
        EntityManagerFactory emf = pu.enFactory;        
        EntityManager em = emf.createEntityManager();
        NUSTUSDApplicantQualification ab= new NUSTUSDApplicantQualification();
        Query query5 = em.createQuery("SELECT n FROM NUSTUSDApplicantQualification n WHERE n.nUSTUSDApplicantQualificationPK.applicantNo = :applicantNo");
        query5.setParameter("applicantNo", search);
        List<NUSTUSDApplicantQualification> resultList5 = query5.getResultList();
        List<NUSTUSDApplicantQualification> qout=new ArrayList<NUSTUSDApplicantQualification>();
        em.close();
        for(NUSTUSDApplicantQualification qq: resultList5){
        if(!qq.getProgrammeDescription().isEmpty()){
        qout.add(qq);
        }
        }
        return qout;
    }
     
        
     /* Find qualifications */
        public List<NUSTUSDApplicantChoice> findChoices(String search){
        System.out.println("Now in findCourses ");
        PersistanceUnit pu = PersistanceUnit.getInstance();
        EntityManagerFactory emf = pu.enFactory;        
        EntityManager em = emf.createEntityManager();
        NUSTUSDApplicantQualification ab= new NUSTUSDApplicantQualification();
        Query query6 = em.createQuery("SELECT n FROM NUSTUSDApplicantChoice n WHERE n.nUSTUSDApplicantChoicePK.applicantNo = :applicantNo");
        query6.setParameter("applicantNo", search);
        List<NUSTUSDApplicantChoice> resultList6 = query6.getResultList();
//        List<NUSTUSDApplicantChoice> qout=new ArrayList<NUSTUSDApplicantChoice>();
//        em.close();
//        for(NUSTUSDApplicantChoice qq: resultList6){
//        if(!qq.getProgrammeDescription().isEmpty()){
//        qout.add(qq);
//        }
//        }
        return resultList6;
    }
        
    public void otherstuff(){
    //        if(findNo(search)!=null){
//        ab=findNo(search);
//        request.setAttribute("Applicant",ab);
//        RequestDispatcher rd = request.getRequestDispatcher("/view_applicant.jsp");
//        rd.forward(request, response);}
//        
//        else if(findCustNo(search)!=null){
//            ab=findCustNo(search);
//        request.setAttribute("Applicant",ab);
//        RequestDispatcher rd = request.getRequestDispatcher("/view_applicant.jsp");
//        rd.forward(request, response);}
//        
//        else if(findSurname(search)!=null){
//        ab=findSurname(search);
//        request.setAttribute("Applicant",ab);
//        RequestDispatcher rd = request.getRequestDispatcher("/view_applicant.jsp");
//        rd.forward(request, response);}
//        
//        else if(findName(search)!=null){
//        ab=findName(search);
//        request.setAttribute("Applicant",ab);
//        RequestDispatcher rd = request.getRequestDispatcher("/view_applicant.jsp");
//        rd.forward(request, response);}
//        
//        else{
//        System.out.println("Search not found");}
    }

}

