/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nust.systems.workingcrud.controllers;

import java.io.IOException;
import java.util.List;
import nust.systems.workingcrud.dao.NUSTUSDApplicantDao;
import nust.systems.workingcrud.entities.NUSTUSDApplicant;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Rodney
 */

@Controller
public class NUSTUSDApplicantController {
    
    NUSTUSDApplicantDao app = new NUSTUSDApplicantDao();
    
    @RequestMapping(value = "/a")
     public ModelAndView getAllFees(ModelAndView model) throws IOException {
        List<NUSTUSDApplicant> getAllApplicants = app.findStudentEntities(50, 0);
        for(NUSTUSDApplicant nts:getAllApplicants){
            
                System.out.println("Student name:" + nts.getForenames() + " " + nts.getCity());
        }
        model.addObject("getAllApplicants", getAllApplicants);
        model.setViewName("index");
        return model;
    }
}
